#include<assert.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include<arpa/inet.h>

#include "delayed_sendto.h"
#include "irq.h"
#include "util.h"

#define PACKETSPERSEC 5
#define SECONDINNSEC 1000000000
#define STEP (SECONDINNSEC/PACKETSPERSEC)
#define MAXLEN 100
#define MAXPACKETS 10


int get_num_packets_in_queue(struct queue_entry *);
struct host_entry* get_existing_host(const struct sockaddr *to, socklen_t to_len);

/**
 * Struct used for stroring information about a
 * remote host and the queue going to that host.
 */
struct host_entry
{
    struct sockaddr  *to;
    socklen_t        to_len;
    int              socket;
    struct queue_entry* head;
    struct queue_entry* tail;
    struct host_entry *next;
};

/**
 * Keeps track of all the hosts.
 */
struct host_entry *hosts = NULL;

/**
 * Keeps track of when the last tick was.
 */

struct timespec prev_tick;

/**
 * Used to initialize the timer for the delayed_sendto() function.
 */
void delayed_send_init(void) {
    if( clock_gettime(CLOCK_REALTIME, &prev_tick) < 0 )
    {
        perror( "error getting time" );
    }
    time_t t;
    time(&t);
    srand48((long int) t);
}

/**
 * Used to free all the memory used by delayed_sendto().
 * You need to call this function youself when you end the program.
 */
void delayed_send_shutdown(void) {
    struct host_entry *host = hosts;
    struct queue_entry *ptr;
    struct host_entry *prev_host;
    while (host != NULL) {
        while( host->head != NULL ) {
            ptr = host->head;
            host->head = host->head->next;
            free( ptr->data );
            free( ptr );
        }
        prev_host = host;
        host = host->next;
        free(prev_host->to);
        free(prev_host);
    }
}



/**
 * Can be used to check the number of packets waiting to be sent to a certain host
 */
int get_num_packets(const struct sockaddr *to, socklen_t to_len) {
    struct host_entry *host = get_existing_host(to, to_len);
    if (host == NULL) {
        return -1;
    }
    return get_num_packets_in_queue(host->head);

}

struct host_entry* get_existing_host(const struct sockaddr *to, socklen_t to_len) {
    struct host_entry *host = hosts;
    while (host != NULL) {
        if (host->to_len == to_len && 
                ((struct sockaddr_in*)to)->sin_addr.s_addr == ((struct sockaddr_in*)(host->to))->sin_addr.s_addr 
                && ((struct sockaddr_in*)to)->sin_port == ((struct sockaddr_in*)(host->to))->sin_port) {
            break;
        }
        host = host->next;
    }
    return host;
}

/**
 * Used interally to get the number of packets in a queue
 */
int get_num_packets_in_queue(struct queue_entry *head) {
    int count = 0;

    while (head != NULL) {
        head = head->next;
        count++;
    }
    return count;
}

/**
 * For the caller, this function behaves exactly like the function sendto(),
 * except that it always returns success and adds delay before actually sending the data.
 *
 * Use this function as described in 'man sendto'.
 * The difference is that all necessary information is stored in a queue
 * and the actual send operation is delayed for a time that is specified
 * in a variable named msg_delay that you find in delayed_sendto.c
 *
 * This means also that delayed_sendto will always return without error.
 */
ssize_t delayed_sendto(int s, const void *msg, size_t len, int flags,
        const struct sockaddr *to, socklen_t to_len)
{
    if (len > MAXLEN) {
        printf("Too long message, got %ld, expected less than %d - dropping.\n", len, MAXLEN);
        return 0;
    }

    struct host_entry *host = get_existing_host(to, to_len);

    if (host == NULL) {
        host = safe_malloc(sizeof(struct host_entry));
        host->to = safe_malloc(to_len);
        host->socket = s;
        memcpy(host->to, to, to_len);
        host->head = host->tail = NULL;
        host->to_len = to_len;
        host->next = hosts;
        hosts = host;
    }

    int count = get_num_packets_in_queue(host->head);

    if (count >= MAXPACKETS) {
        return 0;
    }

    struct queue_entry* ptr = (struct queue_entry*)safe_malloc(sizeof (struct queue_entry));
    assert(ptr);

    ptr->data = safe_malloc(len);
    memcpy( ptr->data, msg, len );
    ptr->data_len = len;

    ptr->next = NULL;

    if( host->tail )
    {
        host->tail->next = ptr;
        host->tail = ptr;
    }
    else
    {
        host->head = host->tail = ptr;
    }
    return len;
}

/**
 * This function sends a packet per STEP passed.
 * Default is 5 packets per second.
 * If dropping is greater than 0, this function will start dropping
 * packets (dropping indicating the percentage of packets lost)
 */
void send_delayed(int dropping)
{
    struct timespec now;
    struct queue_entry *ptr;
    int error;
    long packets_to_send, i;
    int64_t diff;
    if( clock_gettime(CLOCK_REALTIME, &now) < 0 )
    {
        perror( "error getting time" );
        return;
    }
    diff = (now.tv_sec - prev_tick.tv_sec)*SECONDINNSEC + (now.tv_nsec - prev_tick.tv_nsec);



    packets_to_send = diff/STEP;
    if (packets_to_send <= 0) {
        return;
    }

    prev_tick.tv_nsec += packets_to_send*STEP;
    prev_tick.tv_sec += prev_tick.tv_nsec/SECONDINNSEC;
    prev_tick.tv_nsec %= SECONDINNSEC;

    struct host_entry *host = hosts;
    while (host != NULL) {
        i = 0;
        while( host->head && i < packets_to_send)
        {
            if( dropping == 0 || ((lrand48() % 100)+1) > dropping )
            {
                error = sendto( host->socket,
                        host->head->data, host->head->data_len,
                        0,
                        host->to, host->to_len );
                if( error < 0 )
                {
                    perror( "Error sending delayed data" );
                }
            }
            ptr = host->head;
            host->head = ptr->next;
            if( host->head == NULL ) host->tail = NULL;
            free( ptr->data );
            free( ptr );
            i++;
        }
        host = host->next;
    }
}

