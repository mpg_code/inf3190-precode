#ifndef UTIL_H
#define UTIL_H

#include<stdlib.h>
#include<stdio.h>

void *safe_malloc(size_t);

#endif
