#include<assert.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<sys/time.h>
#include<stdlib.h>
#include<string.h>

#include "receiver.h"
#include "util.h"

struct out_file {
    int src;
    int fd;
    struct out_file *next;
};

struct out_file *files = NULL;

int receiver( const char* buf, size_t length, int src, int local_mac )
{
    int err;
    struct out_file *tmp = files;
    int new = 0;
    while (tmp != NULL) {
        if (tmp->src == src) {
            break;
        }
        tmp = tmp->next;
    }

    if (tmp == NULL) {
        tmp = safe_malloc(sizeof(struct out_file));
        tmp->next = files;
        tmp->src = src;
        files = tmp;
        new = 1;
    }

    if( new  == 1)
    {
        char filename[100];
        sprintf(filename, "./inf3190-test-f%d-t%d-XXXXXX", src, local_mac);
        tmp->fd = mkstemp( filename );
        if( tmp->fd < 0 )
        {
            perror("Error opening output file");
            exit(-1);
        }
    }

    err = write( tmp->fd, buf, length );
    if( err < 0 )
    {
        perror( "Error writing to output file" );
        switch( errno )
        {
            case EBADF :
            case EINVAL :
            case EFAULT :
            case EPIPE :
            case ENOSPC :
            case EIO :
                receiver_shutdown();
                exit( -1 );
                break;
            case EAGAIN :
            case EINTR :
            default :
                return 0;
        }
    }
    return 1;
}

/**
 * Frees all the memory used by the receiver.
 */
void receiver_shutdown(void) {
    struct out_file *tmp;
    while (files != NULL) {
        tmp = files;
        files = files->next;
        free(tmp);
    }
}
