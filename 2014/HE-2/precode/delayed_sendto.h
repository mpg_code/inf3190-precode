#ifndef DELAYED_SENDTO_H
#define DELAYED_SENDTO_H

#include <netdb.h>
#include <sys/types.h>

/* for comments see .c file */

/**
 * Every data packet that we want to send with delay is stored in
 * elements of this list type.
 */
struct queue_entry
{
    void*            data;
    size_t           data_len;
    struct queue_entry* next;
};


extern ssize_t delayed_sendto(int s, const void *msg, size_t len,
        int flags,
        const struct sockaddr *to, socklen_t tolen);

void send_delayed(int dropping);
void delayed_send_init(void);
void delayed_send_shutdown(void);
int get_num_packets(const struct sockaddr *, socklen_t);

#endif /* DELAYED_SENDTO_H */
