#include<assert.h>
#include<sys/select.h>
#include<unistd.h>
#include<sys/time.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

/*
 * add includes as you need them
 */
#include "delayed_sendto.h"
#include "receiver.h"
#include "l1_phys.h"
#include "l5_app.h"

#define PACKETLOSS 0




/**
 * This is the main loop of this program.
 * It is meant to emulate a very basic interrupt dispatcher and
 * scheduler of an operating system.
 */
void handle_events(void)
{
    int max_fd = my_udp_socket + 1;
    fd_set          read_set;
    struct timeval  tv;
    int             retval;

    while( 1 )
    {

        /* The fd_set must be cleared and refilled every time before
         * you call select.
         */
        FD_ZERO( &read_set );
        FD_SET( STDIN_FILENO, &read_set );             /* add keyboard input */
        FD_SET( my_udp_socket, &read_set ); /* add UDP socket */

        /* Now wait until something happens.
         */
        tv.tv_sec = 0;
        tv.tv_usec = 100000;
        retval = select( max_fd, &read_set, 0, 0, &tv );

        switch( retval )
        {
        case -1 :
            perror( "Error in select" );
            break;

        case 0 :
            /* Nothing happened on the socket or the keyboard. But the
             * timeout has expired.
             */
	    send_delayed(PACKETLOSS);
            break;

        default :
            /* Something happened on the socket or keyboard. But the
             * timeout may have expired as well. Check that first.
             */
	    send_delayed(PACKETLOSS);

            /* note: when you check several sockets and file descriptors
             *       in an fd_set, always check all of them. If you do an
             *       if-else, one of them may starve.
             */

            /* If file descriptor 0 is set, something has happened on
             * the keyboard. That's most likely user input. Call
             * the application layer directly.
             */
            if( FD_ISSET( STDIN_FILENO, &read_set ) ) l5_handle_keyboard( );

            /* If this file descriptor is set, something has happened on
             * the UDP socket. Probably data has arrived. Call the event
             * handler of the physical layer.
             */
            if( FD_ISSET( my_udp_socket, &read_set ) ) l1_handle_event( );
            break;
        }
    }
}

