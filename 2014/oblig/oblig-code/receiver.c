#include<assert.h>
#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<sys/time.h>
#include<stdlib.h>
#include<string.h>

#include "receiver.h"

static int  testfile = 0;
static char filename[25];

int receiver( const char* buf, int length )
{
    int                   err;

    if( !testfile )
    {
        strcpy( filename, "./inf3190-test-XXXXXX" );
        testfile = mkstemp( filename );
        if( testfile < 0 )
        {
            perror("Error opening output file");
            exit(-1);
        }
    }

    err = write( testfile, buf, length );
    if( err < 0 )
    {
        perror( "Error writing to output file" );
        switch( errno )
        {
            case EBADF :
            case EINVAL :
            case EFAULT :
            case EPIPE :
            case ENOSPC :
            case EIO :
                exit( -1 );
                break;
            case EAGAIN :
            case EINTR :
            default :
                return 0;
        }
    }
    return 1;
}

