#ifndef RECEIVER_H
#define RECEIVER_H

/**
 * This function writes length bytes from buf to a file in the current
 * directory with a random name.
 * The function returns 1 if the bytes contained in buf are written
 * correctly to disk.
 * It returns 0 if it could not write the bytes to disk. 
 */
int receiver( const char *buf, int length );

#endif /* SLOW_NDEMUX_H */

