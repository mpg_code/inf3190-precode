#include "util.h"


/**
 * A safe malloc-function which terminates the program should 
 * malloc return a NULL-pointer (as this may happen on a server).
 */
void *safe_malloc(size_t s) {
    void *ptr = malloc(s);
    if (ptr == NULL) {
        perror("Malloc has returned a NULL-pointer.");
        exit(-1);
    }
    return ptr;
}
